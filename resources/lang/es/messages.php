<?php

return [
    'Welcome' => 'Bienvenido',
    'Register' => 'Registro',
    'Name' => 'Nombre',
    'Surname' => 'Apellidos',
    'Email' => 'Correo electrónico',
    'Password' => 'Contraseña',
    'Confirm Password' => 'Confirma contraseña'
];

?>
