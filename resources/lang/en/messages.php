<?php

return [
    'Welcome' => 'Welcome',
    'Register' => 'Register',
    'Name' => 'Name',
    'Surname' => 'Surname',
    'Email' => 'Email',
    'Password' => 'Password',
    'Confirm Password' => 'Confirm Password'
];

?>
