<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('products')->insert([
            'nombre' => 'Botas',
            'precio' => 45,
            'stock' => 10,
            'descripcion' => 'description',
            'image' => base64_encode(file_get_contents('https://images-na.ssl-images-amazon.com/images/I/81qqYg8ntkL._AC_UY395_.jpg'))
        ]);

        DB::table('products')->insert([
            'nombre' => 'Sneakers Unisex',
            'precio' => 150,
            'stock' => 5,
            'descripcion' => 'description',
            'image' => base64_encode(file_get_contents('https://www.versace.com/dw/image/v2/ABAO_PRD/on/demandware.static/-/Sites-ver-master-catalog/default/dwe6a48765/original/90_DST030G-DT21_DBN9_20_BaroccoPrintChainReaction2Trainers-Sneakers-versace-online-store_3_1.jpg?sw=850&sh=1200&sm=fit'))
        ]);

        DB::table('products')->insert([
            'nombre' => 'Camiseta',
            'precio' => 50,
            'stock' => 15,
            'descripcion' => 'description',
            'image' => base64_encode(file_get_contents('https://www.versace.com/on/demandware.static/-/Sites-ver-master-catalog/default/dw3922b469/original/90_A79160-A224620_A92Y_10_EmbellishedMedusaLogoT-Shirt-T-shirtsandPolos-versace-online-store_7_6.jpg'))
        ]);

    }
}
