<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Items;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    //

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function mostrar(Request $request){

        $value = empty($request->input('price'))?'0':$request->input('price') ;
        $valueFilter = empty($request->input('filter'))?'NONE':$request->input('filter') ;

        if (!strcmp($valueFilter, "NONE")){
            $all = DB::select(DB::raw("select * from products"));
        }else{
            if (strcmp($valueFilter,"MAX")){
                $all = DB::select(DB::raw("select * from products where precio >= $value"));
            }elseif (strcmp($valueFilter, "MIN")){
                $all = DB::select(DB::raw("select * from products where precio <= $value"));
            }

        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 2;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $productos= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $productos->setPath($request->url());
        return view('products')->with('productos',$productos);
    }

    public function addToCart($id){

        $product = Product::find($id);
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id",$user)->first();

        if ( empty($cart_db) ){
            $cart_db = new Cart();
            $cart_db->user_id = $user;
            $cart_db->save();
        }

        if(!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        if(!$cart) {
            $cart = [
                $id => [
                    "nombre" => $product->nombre,
                    "cantidad" => 1,
                    "precio" => $product->precio,
                    "image" => $product->image ]
            ];

            $cart_item = new Cart_Items();
            $cart_item->cart_id=$cart_db->id;
            $cart_item->product_id=$id;
            $cart_item->amount=1;
            $cart_item->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if(isset($cart[$id])) {
            $cart[$id]['cantidad']++;
            $cart_item = Cart_Items::where([
                ['cart_id',$cart_db->id],
                ['product_id',$id],
            ])->first();
            $cart_item->amount=$cart[$id]['cantidad'];
            $cart_item->update();
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "nombre" => $product->nombre,
            "cantidad" => 1,
            "precio" => $product->precio,
            "image" => $product->image
        ];

        $cart_item = new Cart_Items();
        $cart_item->cart_id=$cart_db->id;
        $cart_item->product_id=$id;
        $cart_item->amount=1;
        $cart_item->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function checkCart(){
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id",$user)->first();

        $cart_items = DB::select(DB::raw("select * from cart_items where cart_id = $cart_db->id"));

        foreach ($cart_items as $item){
            $id = $item->product_id;
            $product = Product::find($id);
            $cart[$id] = [
                "nombre" => $product->nombre,
                "cantidad" => $item->amount,
                "precio" => $product->precio,
                "image" => $product->image
            ];
            session()->put('cart', $cart);
        }
    }

    public function cart()
    {
        return view('cart');
    }

    public function update(Request $request){
        if($request->id and $request->cantidad){
            $cart = session()->get('cart');
            $cart[$request->id]["cantidad"] = $request->cantidad;

            $user = Auth::user()->id;
            $cart_db = Cart::where("user_id",$user)->first();
            $cart_item = Cart_Items::where([
                ['cart_id',$cart_db->id],
                ['product_id',$request->id],
            ])->first();

            $cart_item->amount=$request->cantidad;
            $cart_item->update();

            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request){
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);

                $user = Auth::user()->id;
                $cart_db = Cart::where("user_id",$user)->first();
                $cart_item = Cart_Items::where([
                    ['cart_id',$cart_db->id],
                    ['product_id',$request->id],
                ])->first();
                $cart_item->delete();

                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function venta(){
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id",$user)->first();
        $cart = session()->get('cart');
        $product_id = array_keys($cart);

        foreach ($product_id as $id){
            $product = Product::find($id);
            $stock = $product->stock;
            $cantidadToComprar = $cart[$id]["cantidad"];
            if ($cantidadToComprar > $stock){
                return redirect()->back()->with('error', 'La cantidad solicitada supera al sotck para '.$product->nombre);
            }else{
                $product->stock = ($stock-$cantidadToComprar);
                $product->update();

                DB::table('cart_items')->where('cart_id', '=', $cart_db->id)->delete();

                session()->remove('cart');

                return redirect()->back()->with('success', 'Se ha completado el pago :)');
            }
        }


    }

}
